from django.test import Client, TestCase
from django.urls import reverse_lazy

from forum.common.test_mixins import TestMixin
from forum.common.models import Category, Topic


class TestTopicBlockView(TestMixin, TestCase):
    def setUp(self):
        super().setUp()
        self.client = Client()
        category, _ = Category.objects.get_or_create({
            'name': 'Test category2',
            'order_number': 2,
        })
        self.topic, _ = Topic.objects.get_or_create({
            'name': 'Test category2',
            'category_id': category.pk,
        })

    def test_topic_block_view_not_loading_for_unauthenticated_user(self):
        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertEqual(302, response.status_code)
        self.assertTrue('/login' in response.url)

    def test_topic_block_view_loading_for_super_user(self):
        user = self.get_or_create_superuser()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertContains(response, 'Are you sure you want block topic?', status_code=200)
        self.assertContains(response, user.username)

    def test_topic_block_view_loading_for_user(self):
        user = self.get_or_create_user()
        self.client.force_login(user)

        topic = self.topic
        response = self.client.get(reverse_lazy('common:block_topic', kwargs={'pk': topic.pk}))

        self.assertEqual(403, response.status_code)
